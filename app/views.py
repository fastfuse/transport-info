
# -*- coding: utf-8 -*-

from app import app, db, admin
from flask import request, url_for, redirect, render_template
from flask.ext.admin.contrib.sqla import ModelView
import requests
import models


admin.add_view(ModelView(models.Stop, db.session))


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', stops=models.Stop.query.all())


@app.route('/search', methods=['GET', 'POST'])
def search():
    return redirect(url_for('show', stop=request.form['stop_id']))


@app.route('/show/<string:stop>', methods=['GET', 'POST'])
def show(stop):
    url = 'http://lad.lviv.ua/api/timetable/{}'.format(stop)
    answer = requests.get(url)

    json_answer = answer.json()

    return render_template('info.html', data=json_answer,
                           stops=models.Stop.query.all(),
                           stop_info=models.Stop.query.
                           filter_by(code=stop).first())
