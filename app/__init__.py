
import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.admin import Admin


app = Flask(__name__)

admin = Admin(app)

# app.config.from_pyfile('config.py')
app.config.from_object(os.environ['APP_SETTINGS'])

db = SQLAlchemy(app)

from app import views
