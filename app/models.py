
# -*- coding: utf-8 -*-

from app import db
from datetime import datetime, timedelta


class Stop(db.Model):
    __tablename__ = 'stops'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    code = db.Column(db.String)

    def __init__(self, name="", longitude=0, latitude=0, code=""):
        self.name = name
        self.longitude = longitude
        self.latitude = latitude
        self.code = code

    def __repr__(self):
        return self.name


class Test(db.Model):
    __tablename__ = 'test'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __init__(self, name=""):
        self.name = name

    def __repr__(self):
        return self.name
